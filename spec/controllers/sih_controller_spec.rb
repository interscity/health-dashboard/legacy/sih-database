require 'rails_helper'
require 'spec_helper'

describe Api::V1::SihController, type: 'controller' do
	describe 'Testing getSih method' do
		before :each  do
			Sih.create id: 1, CD_GEOCODI: 201030, LONG_SC: 23.543
			Sih.create id: 2, CD_GEOCODI: 1, LONG_SC: 23.11
			Sih.create id: 3, CD_GEOCODI: 1, LONG_SC: 10.11, LAT_SC: 10.5
		end

		it 'should return nil for a call without parameters' do
			controller.send :updateSession
			controller.send :getSih
			expect(assigns(:sih)).to eq(nil)
		end

		it 'should return all values for a call with send_all parameters' do
			filters = ["201030"]
			controller.params[:send_all] = "true"
			controller.params[:filters] = filters
			controller.send :updateSession
			controller.send :getSih
			expect(assigns(:sih)).to eq(Sih.all)
		end

		it 'should return empty active record to a call without matching parameters' do
			filters = ["12455"]
			controller.params[:filters] = filters
			controller.send :updateSession
			controller.send :getSih
			expect(assigns(:sih)).to eq(Sih.where(:CD_GEOCODI => "12455"))
		end

		it 'should return the correct procedure for CD_GEOCODI' do
			filters = ["201030"]
			controller.params[:filters] = filters
			controller.send :updateSession
			controller.send :getSih
			expect(assigns(:sih)).to eq(Sih.where(:CD_GEOCODI => "201030"))
		end

		it 'should return the correct procedure for LONG_SC' do
			filters = ["", "23.11"]
			controller.params[:filters] = filters
			controller.send :updateSession
			controller.send :getSih
			expect(assigns(:sih)).to eq(Sih.where(:LONG_SC => "23.11"))
		end

		it 'should return the correct procedure for LAT_SC' do
			filters = ["", "", "10.5"]
			controller.params[:filters] = filters
			controller.send :updateSession
			controller.send :getSih
			expect(assigns(:sih)).to eq(Sih.where(:LAT_SC => "10.5"))
		end
	end
end