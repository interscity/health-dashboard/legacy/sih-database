Rails.application.routes.draw do
	scope module: 'api' do
		namespace :v1 do
			resources :sih do
				get :heartBeat, on: :collection
				get :clusterData, on: :collection
				get :clusterPointIds, on: :collection
				get :pointInfo, on: :collection
				get :download, on: :collection
				get :filtersName, on: :collection
				get :filtersValues, on: :collection
				get :filtersHints, on: :collection
				get :shapesNames, on: :collection
				get :shapesUrls, on: :collection
				get :SP, on: :collection
				get :CRS, on: :collection
				get :STS, on: :collection
				get :DA, on: :collection
			end
		end
	end
end
