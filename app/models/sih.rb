class Sih < ApplicationRecord
	acts_as_mappable :lat_column_name => :LAT_SC,
					 :lng_column_name => :LONG_SC
	acts_as_copy_target

	def calculate_distance
		self.distance_to([self.LAT_CNES, self.LONG_CNES], {units: :kms})
	end
end
