module Api::V1
	class SihController < ApiController
		before_action :updateSession, :getSih, only: [:clusterData, :clusterPointIds, :download]
		after_action :sihCleanup, only: [:clusterData, :clusterPointIds, :download]

		def initialize
			@sih_names = ["CD_GEOCODI", "LONG_SC", "LAT_SC", "ESPECIALID", "CMPT", "DT_EMISSAO", "DT_INTERNA", "DT_SAIDA", 
						  "PROC_RE", "CAR_INTEN", "DIAG_PR", "DIAG_SE1", "DIAG_SE2", "P_SEXO", "P_RACA", "P_IDADE", 
						  "LV_INSTRU", "GESTOR_IDE", "DIARIAS", "DIARIAS_UT", "DIARIAS_UI", "COMPLEXIDA", "FINACIAME", 
						  "VAL_TOT", "DIAS_PERM", "CNES", "CNES_DENO", "CNES_DENOR", "LONG_CNES", "LAT_CNES", "COD_GISA", 
						  "COD_DA", "DA", "PR", "STS", "CRS", "DISTANCE"]
			@sih = nil
		end

		def heartBeat
			render json: "API is online, serving SIH data", status: 200 and return
		end

		def clusterData
			if @sih == nil
				render json: "Bad Request", status: 400
			else
				clusterData = @sih.group(:LAT_SC, :LONG_SC).count.to_a.flatten.each_slice(3)

				render json: clusterData, status: 200
			end
		end

		def clusterPointIds
			render json: "Bad request", status: 400 and return unless @sih != nil

			clusterpoints = @sih.where(:LAT_SC => params[:lat], :LONG_SC => params[:long]).pluck(:id)
			render json: clusterpoints, status: 200
		end

		def pointInfo
			sih = Sih.where(id: params[:id]).to_a

			render json: sih, status: 200
		end

		def download
			@sih = Sih.all
			render json: "Bad request", status: 400 and return unless @sih != nil

			enumerator = @sih.copy_to_enumerator(:buffer_lines => 100, :delimiter => ";")
			# Tell Rack to stream the content
			headers.delete("Content-Length")

			# Don't cache anything from this generated endpoint
			headers["Cache-Control"] = "no-cache"

			# Tell the browser this is a CSV file
			headers["Content-Type"] = "text/csv"

			# Make the file download with a specific filename
			headers["Content-Disposition"] = "inline; filename=\"internacoes-hospitalares-#{Date.today}.csv\""

			# Don't buffer when going through proxy servers
			headers["X-Accel-Buffering"] = "no"

			# Set an Enumerator as the body
			self.response_body = enumerator

			# Set the status to success
			response.status = 200
		end

		# GET /v1/filters_name
		def filtersName
			render json: @sih_names, status: 200
		end

		def filtersValues
			filter_value = [[], [], [], [], [], [{"id" => "A00","text" => "A00 - Cólera"}, {"id" => "A01","text" => "A01 - Febres Tifóide E Paratifóide"}, {"id" => "A02","text" => "A02 - Outras Infecções Por Salmonella"}], [], [], [], [], [], [], [], [], [], [], [], [], []]
			render json: filter_value, status: 200
		end

		def filtersHints
			filters_hints = ["Estabelecimento do atendimento prestado.", "Ano/mês de processamento da AIH. Ex: 201506(junho de 2015).", "Grupo do procedimento autorizado ao paciente.", "Especialidade do leito de internação.", "Caráter da internação.", "Motivo da internação.", "Motivo que levou ao diagnóstico principal.", "Motivo que levou ao diagnóstico principal.", "Motivo que levou ao diagnóstico principal.", "Nível de atenção para realização do procedimento.", "Tipo de financiamento do procedimento.", "", "", "", "", "", "", "", ""]
			render json: filters_hints, status: 200
		end

		def shapesNames
			shapes_name = ["Municipio", "CRS", "STS", "DA"]
			render json: shapes_name, status: 200
		end

		def shapesUrls
			shapes_url = ["http://localhost:3030/v1/sih/SP", "http://localhost:3030/v1/sih/CRS", "http://localhost:3030/v1/sih/STS", "http://localhost:3030/v1/sih/DA"]
			render json: shapes_url, status: 200
		end

		def SP
			sp = JSON.parse(File.read(Rails.root.join('public/Shape_SP.geojson')))
			render json: sp, status: 200
		end

		def CRS
			crs = JSON.parse(File.read(Rails.root.join('public/Shape_CRS.geojson')))
			render json: crs, status: 200
		end

		def STS
			sts = JSON.parse(File.read(Rails.root.join('public/Shape_STS.geojson')))
			render json: sts, status: 200
		end

		def DA
			da = JSON.parse(File.read(Rails.root.join('public/Shape_DA.geojson')))
			render json: da, status: 200
		end

	private
		def updateSession
			if params[:send_all] != nil
				session[:send_all] = params[:send_all].dup
			end
			if params[:filters] != nil
				session[:filters] = params[:filters].dup
			end
			return
		end

		def getSih
			@sih = Sih.where(nil)

			(@sih = Sih.all; return) if session[:send_all] == "true"

			(@sih = nil; return) if session[:filters] == nil

			@sih_names.each.with_index do |filter, index|
				if session[:filters][index] == nil || session[:filters][index] == ""
					next
				end

				data = session[:filters][index].split(";")
				@sih = @sih.where(filter => data)
			end

			@sih = nil unless @sih != Sih.where(nil)
		end

		def sihCleanup
			@sih = nil
		end
	end
end