require 'json'
require 'csv'

def updateDistance
	path = File.join(Rails.root.join("distance_lastcnes.csv"))

	CSV.foreach(path, :headers => true) do |row|
		procedures = Sih.where(LONG_SC: row[0], LAT_SC: row[1], LONG_CNES: row[2], LAT_CNES: row[3])
		procedures.each do |p|
			p.DISTANCE = row[4].to_f
			p.save!
			print "."
		end
	end
	puts "Done"
end

updateDistance()