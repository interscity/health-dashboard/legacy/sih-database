require 'csv'

def populate_sih()
  puts 'Saving sih data.'
  items = []
  sih_counter = 0
  counter = 0
  sih_csv_path = File.join(__dir__, "csv/v2_procedures.csv")
  CSV.foreach(sih_csv_path, headers: true) do |row|
    items << Sih.new(row.to_h)
    counter += 1
    if counter == 1000
      Sih.import items
      items = []
      sih_counter += counter
      counter = 0
      progress = ((sih_counter.to_f / 554202.0) * 100.0)
      printf("\rProgress: [%-100s] %d%%", "=" * progress, progress)
    end
  end
  Sih.import items
  printf("\rProgress: [%-100s] %d%%", "=" * 100, 100)
  sih_counter += counter
  puts ""
  puts "#{sih_counter} rows successfully saved"
end

populate_sih()