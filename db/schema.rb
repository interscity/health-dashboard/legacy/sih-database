# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180831164427) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "sihs", force: :cascade do |t|
    t.string   "CD_GEOCODI"
    t.float    "LONG_SC"
    t.float    "LAT_SC"
    t.integer  "ESPECIALID"
    t.integer  "CMPT"
    t.date     "DT_EMISSAO"
    t.date     "DT_INTERNA"
    t.date     "DT_SAIDA"
    t.integer  "PROC_RE"
    t.integer  "CAR_INTEN"
    t.string   "DIAG_PR"
    t.string   "DIAG_SE1"
    t.string   "DIAG_SE2"
    t.string   "P_SEXO"
    t.integer  "P_RACA"
    t.integer  "P_IDADE"
    t.integer  "LV_INSTRU"
    t.integer  "GESTOR_IDE"
    t.integer  "DIARIAS"
    t.integer  "DIARIAS_UT"
    t.integer  "DIARIAS_UI"
    t.integer  "COMPLEXIDA"
    t.integer  "FINACIAME"
    t.float    "VAL_TOT"
    t.integer  "DIAS_PERM"
    t.integer  "CNES"
    t.string   "CNES_DENO"
    t.string   "CNES_DENOR"
    t.float    "LONG_CNES"
    t.float    "LAT_CNES"
    t.integer  "COD_GISA"
    t.integer  "COD_DA"
    t.string   "DA"
    t.string   "PR"
    t.string   "STS"
    t.string   "CRS"
    t.float    "DISTANCE"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
