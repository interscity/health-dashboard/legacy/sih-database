class CreateSihs < ActiveRecord::Migration[5.0]
  def change
    create_table :sihs do |t|
    	t.string :CD_GEOCODI
    	t.float :LONG_SC
    	t.float :LAT_SC
    	t.integer :ESPECIALID
    	t.integer :CMPT
    	t.date :DT_EMISSAO
    	t.date :DT_INTERNA
    	t.date :DT_SAIDA
    	t.integer :PROC_RE
    	t.integer :CAR_INTEN
    	t.string :DIAG_PR
    	t.string :DIAG_SE1
    	t.string :DIAG_SE2
    	t.string :P_SEXO
    	t.integer :P_RACA
    	t.integer :P_IDADE
    	t.integer :LV_INSTRU
    	t.integer :GESTOR_IDE
    	t.integer :DIARIAS
    	t.integer :DIARIAS_UT
    	t.integer :DIARIAS_UI
    	t.integer :COMPLEXIDA
    	t.integer :FINACIAME
    	t.float :VAL_TOT
    	t.integer :DIAS_PERM
    	t.integer :CNES
    	t.string :CNES_DENO
    	t.string :CNES_DENOR
    	t.float :LONG_CNES
    	t.float :LAT_CNES
    	t.integer :COD_GISA
    	t.integer :COD_DA
    	t.string :DA
    	t.string :PR
    	t.string :STS
    	t.string :CRS
    	t.float :DISTANCE
    	t.timestamps
    end
  end
end
