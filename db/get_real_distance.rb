require 'rest-client'
require 'json'

def getRealDistance
	lat_long = Sih.group(:LAT_SC, :LONG_SC, :LAT_CNES, :LONG_CNES).order("count_all DESC").count
	puts "LONG_SC,LAT_SC,LONG_CNES,LAT_CNES,distance"
	lat_long.each do |latlng|
		begin
			response = RestClient.get("localhost:5000/route/v1/driving/" + latlng[0][1].to_s + "," + latlng[0][0].to_s + ";" + latlng[0][3].to_s + "," + latlng[0][2].to_s + "?overview=false")
		rescue RestClient::BadRequest
			next
		end
		real_distance = 0
		parsed = JSON.parse response
		routes = parsed["routes"]
		real_distance = (routes[0]["distance"].to_f / 1000).round(1)
		puts latlng[0][1].to_s + "," + latlng[0][0].to_s + "," + latlng[0][3].to_s + "," + latlng[0][2].to_s + "," + real_distance.to_s
	end
end

getRealDistance()